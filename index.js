// alert("h1dfasda");

// Mock database
let posts = [];

// Post ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the  "create" button.

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from reloading. Also prevents the default behavior of our event.
	e.preventDefault();

	posts.push({
		// id prorpery will be used for the unique ID of each posts.
		id: count,

		// "title" and "body" values will come from the "form-add-post"
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	// count will increment everytime a new movie is posted.
	count++

	console.log(posts);
	alert("Movie Successfully Added!");

	showPosts();
});

// 2022 | Action/Adventure

// RETRIEVE POST

const showPosts = () => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `

			<div id="post-${post.id}"> 
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});

	// To check what is stored in the ponstEntrties variable
	console.log(postEntries);

	// To assign the values of postEntries to the element "with divs-post-enteries" id
	document.querySelector("#div-post-entries").innerHTML = postEntries
}


// EDIT POST DATA / EDIT BUTTON

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};


// Update post (Update button)
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts);
			alert("Successfully updated!");
			break;
		}
	}
});


const deletePost = (id) => {
  for (let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === id.toString()) {
			posts.splice(i, 1);

			showPosts(posts);
			alert("Successfully deleted!");
			break;
		}
	}
}